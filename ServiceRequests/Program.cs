﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;
using System.Threading;

namespace ServiceRequests
{
    class ClientService
    {
        private Random client_random = new Random();
        private int SleepTick;
        private TcpClient client;
        public ClientService(TcpClient CliService, int Tick=500)
        {
            client = CliService;
            SleepTick = Tick;
            StartHandler();
        }

        public void StartHandler()
        {
            int count;
            byte[] BufferClient = new byte[1024];
            string info_task;
            int count_task = 0;
            while ((count = client.GetStream().Read(BufferClient, 0, BufferClient.Length)) > 0)
            {
                info_task = Encoding.ASCII.GetString(BufferClient, 0, count);
                var my_task = info_task.Split(';');
                string name_task = my_task[0].Split(':')[1];
                string args_task = my_task[1].Split(':')[1];
                if (name_task == "sum")
                {
                    List<Int32> args = new List<Int32>();
                    foreach (string arg in args_task.Split(','))
                    {
                        int arg_task = Convert.ToInt32(arg);
                        args.Add(arg_task);
                    }
                    int result = add(args);
                    count_task++;
                    byte[] buffer_result = Encoding.ASCII.GetBytes("Result Task " + count_task.ToString() + ": " + result.ToString());
                    client.GetStream().Write(buffer_result, 0, buffer_result.Length);
                }
            }
            client.Close();
        }

        public int add(List<Int32> args)
        {
            int result;
            result = args.Sum();
            Thread.Sleep(SleepTick);
            return result;
        }
    }

    class ServerRequest
    {
        TcpListener Listener;
        private static List<TcpClient> ClientsServer;
        private static int SleepClient = 500;
        public ServerRequest(string ip_address = "127.0.0.1", int port = 1000)
        {
            IPAddress address;

            if (IPAddress.TryParse(ip_address, out address))
            {
                Listener = new TcpListener(address, port);
                Listener.Start();
                Console.WriteLine("SERVER STARTER IP: " + address + "\nPORT: " + port.ToString() + "\n\n");
                while (true)
                {
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ClientThread), Listener.AcceptTcpClient());
                }
            }
            else
            {
                throw new System.ArgumentException("Ip address is not correct, please relaunch service", "ip_address");
            }
        }

        static void ClientThread(Object StateInfoClient)
        {
            ClientService CliService;
            TcpClient client = (TcpClient)StateInfoClient;
            try
            {
                Console.WriteLine("\nClients connected service: {0}\n", ClientsServer.Count + 1);
                Console.WriteLine("Client '{0}' connection established.", client.Client.RemoteEndPoint);
                if (ClientsServer.Count == 0)
                {
                    SleepClient = 500;
                    ClientsServer.Add(client);
                    CliService = new ClientService(client, SleepClient);
                    
                }
                else
                {
                    SleepClient += 500;
                    ClientsServer.Add(client);
                    CliService = new ClientService(client, SleepClient);

                }
            }
            catch (Exception e)
            {
                ClientsServer.Remove(client);
                SleepClient -= 500;
                Console.WriteLine("Client '{0}' connection lost.", client.Client.RemoteEndPoint);
            }
        }

        ~ServerRequest()
        {
            if (Listener != null)
            {
                Listener.Stop();
                Console.WriteLine("Сервис остановлен...");
            }
        }

        static void Main(string[] args)
        {
            string address;
            int port;
            int MaxThreadCount = Environment.ProcessorCount * 4;
            ThreadPool.SetMaxThreads(MaxThreadCount, MaxThreadCount);
            ThreadPool.SetMinThreads(2, 2);
            ClientsServer = new List<TcpClient>(MaxThreadCount);
            Console.WriteLine("INITILIAZE SERVER...\nINIT CONFIG: MaxThreadCount={0}", MaxThreadCount.ToString());
            if (args.Length != 0 && args.Length == 2)
            {
                address = args[0];
                try
                {
                    port = Convert.ToInt32(args[1]);
                    new ServerRequest(address, port);
                }
                catch (FormatException e) { Console.WriteLine("PORT SERVER IS WRONG. Error_msg: " + e.Message); }
            }
            else
            {
                new ServerRequest();
            }
        }
    }
}
