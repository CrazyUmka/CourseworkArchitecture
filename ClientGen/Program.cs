﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace GeneratorRequests
{
    class ClientGen
    {
        private TcpClient Client;
        private Random my_gen = new Random();

        public ClientGen(string address = "127.0.0.1", int port = 1000)
        {
            byte[] my_buffer;
            int count;
            Console.WriteLine("Try connect server: {0}:{1}.", address, port.ToString());
            Client = new TcpClient(address, port);
            Console.WriteLine("Connection Established...");
            my_buffer = GetEncMsg();
            byte[] BufferRead = new byte[1024];
            Client.GetStream().Write(my_buffer, 0, my_buffer.Length);

            while ((count = Client.GetStream().Read(BufferRead, 0, BufferRead.Length)) > 0)
            {
                Console.WriteLine(Encoding.ASCII.GetString(BufferRead, 0, count));
                my_buffer = GetEncMsg();
                Client.GetStream().Write(my_buffer, 0, my_buffer.Length);
            }
            Client.Close();
        }

        private byte[] GetEncMsg()
        {
            string query_req = String.Format("task:sum;args:{0},{1}",
                                             my_gen.Next(100000).ToString(),
                                             my_gen.Next().ToString());
            return Encoding.ASCII.GetBytes(query_req);
        }

        ~ClientGen()
        {
            if (Client != null)
            {
                Client.Close();
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("INIT CLIENT GENERATOR...");
            try
            {
                new ClientGen();
            }
            catch (Exception e)
            {
                Console.WriteLine("Server is not response. Please check server.");
            }
            Console.ReadLine();
        }
    }
}
